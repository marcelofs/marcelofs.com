docker run -d \
  --name blog \
  -v /docker/blog:/var/lib/ghost \
  ghost

docker run -d \
  --name nginx  \
  --link blog:blog \
  -v /docker/nginx/conf.d:/etc/nginx/conf.d \
  -v /docker/nginx/html:/etc/nginx/html \
  -v /docker/nginx/logs:/var/log/nginx \
  -v /docker/nginx/cache:/var/cache/nginx \
  -p 80:80 \
  nginx

# nano /etc/environment
# ...
# export MADRUGA_PASS=""
# ...
# source /etc/environment to refresh shell
docker run -d \
  --name madruga \
  -e MADRUGA_PASS=$MADRUGA_PASS \
  -e MADRUGA_GOOGLE_API_KEY=$MADRUGA_GOOGLE_API_KEY \
  -e MADRUGA_FIREBASE_PASS=$MADRUGA_FIREBASE_PASS \
  -e MADRUGA_IMPORTIO_API=$MADRUGA_IMPORTIO_API \
  -e MADRUGA_CHANNELS="#vamojunto #defesa.br #ebr #brozil" \
  -e MADRUGA_DEBUG=true \
  -p 50555:50555 \
  marcelofs/madrugabot 